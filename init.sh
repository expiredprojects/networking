#!/usr/bin/env bash

echo deb http://archive.ubuntu.com/ubuntu/ precise main restricted > /etc/apt/sources.list
apt-get update
apt-get install -y squid3
apt-get install -y apache2-utils
sudo htpasswd -bmc /etc/squid3/passwd jj 1234
sudo chmod o+r /etc/squid3/passwd
cp /vagrant/squid.conf /etc/squid3/squid.conf
sudo service squid3 restart
